import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  const player1 = {
    ...firstFighter,
    playerState: 'waiting'
  }

  const player2 = {
    ...secondFighter,
    playerState: 'waiting'
  }

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    function hit(attacker, defender) {
      const damage = getDamage(attacker, defender)
      if(attacker.playerState !== 'blocking'){
        defender.health -= damage
      }
      if(defender.health <= 0){
        attacker.playerState = 'winner';
        defender.playerState = 'loser';
        resolve(attacker)
      } else {
        attacker.playerState = 'waiting';
        defender.playerState = 'waiting';
      }
    }

    function block(defender) {
      defender.playerState = 'blocking'
    }

    const onClick = (event, firstFighter, secondFighter) => {

      switch (event.code) {
        case controls.PlayerOneAttack:
        // use settimeout in order to give another player time to get block
          setTimeout(
            () => hit(firstFighter, secondFighter),
            500
        )
          break;
        case controls.PlayerOneBlock:
          block(firstFighter)
          break;
        case controls.PlayerTwoAttack:
        // use settimeout in order to give another player time to get block
          setTimeout(
            () => hit(secondFighter, firstFighter),
            500
        )
          break;
        case controls.PlayerTwoBlock:
          block(secondFighter)
          break
        default:

      }
    }
    document.addEventListener('keydown', () => onClick(event, player1, player2))
  });
}


export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}
