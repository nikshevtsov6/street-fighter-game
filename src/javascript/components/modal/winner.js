import { showModal } from './modal';

import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const title = `WINNER ${fighter.name}`;
  const imageElement = createFighterImage(fighter);
  const bodyElement = createElement({
    tagName: 'div',
    className: 'fighter-winner',
  });
  bodyElement.append(imageElement)
  showModal({title, bodyElement})
}
