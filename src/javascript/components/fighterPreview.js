import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if(fighter){
    const imageElement = createFighterImage(fighter);
    const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
    nameElement.innerText = `Name: ${fighter.name}`;
    const healthElement = createElement({ tagName: 'div', className: 'fighter-health' });
    healthElement.innerText = `Health: ${fighter.health}`;
    const attackElement = createElement({ tagName: 'div', className: 'fighter-attack' });
    attackElement.innerText = `Attack: ${fighter.attack}`;
    const defenseElement = createElement({ tagName: 'div', className: 'fighter-defense' });
    defenseElement.innerText = `Defense: ${fighter.defense}`;
    fighterElement.append(imageElement, nameElement, healthElement, attackElement, defenseElement);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
